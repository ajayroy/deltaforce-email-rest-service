/**
 *
 */
package com.deltaforceanalytics.serviceImpl;


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;



public class DeltaForcemailManagerImpl{

	/**
	 * Generic Method to send an email.
	 * @return
	 */
	public  boolean sendCreateAccountMessage(String reciever,String name,String Password){

		try {
			  // Recipient's email ID needs to be mentioned.
		      String to = reciever;

		      // Sender's email ID needs to be mentioned
		      String from = "postmaster@deltaforceanalytics.com";

		      // Assuming you are sending email from localhost
		      String host = "mail.deltaforceanalytics.com";
		      
		    //  final String username = "sanjayuit@gmail.com";
				//final String password = "g9m8a2i6l4";
		 
		      	final String username = "postmaster@deltaforceanalytics.com";
		      	final String password = "Deltaforce2013";
		      	
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				//props.put("mail.smtp.host", "smtp.gmail.com");
				props.put("mail.smtp.host", host);
				props.put("mail.smtp.port", "587");
		 			//System.out.println("emailRequest.getSendersEmail : " + emailRequest.getSenderEmail());
				Session session = Session.getInstance(props,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				  });
		 
				
		 
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(from));
					message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(to));
					message.setSubject("DeltaForceAnalytics-Account Created Successfully");
					//message.setText("");
					String textMsg = "<div style=\"background-color:#444;color:#fff;width:626px;height:300px;margin-left:7%;\">"+"<div style=\"margin:0 auto;line-height:18px;padding:0;text-align:center;font-size:12px;font-family:Tahoma,Verdana,Arial,sans-serif;width:600px;\">"+"<div style=\"background-image: linear-gradient(#444, #111);-moz-margin-start:-12px;-webkit-margin-start:-12px;padding:10px;text-align:left;\"><img style=\"border:0px; margin-top: 1%; width=205; height=32;\" src=\"http://dfa-webapp-619760838.us-east-1.elb.amazonaws.com/images/logo.png\" ></div>"+" <div style=\"color:#d2aa56;font-size:16px;padding:8px;text-align:left;\">"
							+"Your Account has been Created Successfully!"+"</div>"+"<div style=\"margin-top:20px;color:#fff;text-align:left;margin-left:20px;\">"+" <span>Dear "+name+",</span>"
							+"<p ><span style=\"color:#fff;\"> Thank you for Register with us,&nbsp;&nbsp;"
							+" Your login email ID:</span>"+" <span>"+"<a style=\"color:#00ca00;\">"+ reciever+"</a>"+"</span>"+"<br>"
							+" Your login password:"+" <span style=\"color:#00ca00;\">"+ Password+"</span>"+"</p>"
							+"<p style=\"color:#fff;\">In case you have questions or need further assistance, call us on "+"<a style=\"color:#00ca00;\" >"+"+91 9999999999"+"</a>"+" or"+"&nbsp;email us on <a style=\"color:#00ca00;\" >"+"postmaster@deltaforceanalytics.com"+"</a>"+" We would be glad to help you."+"</p>"
	                        + "<p style=\"color:#fff;\">"+" Sincerely,<br>"+" The DeltaForce Team"+"</p>"+"</div>"+"</div>";
					message.setContent(textMsg, "text/html");
			
					Transport.send(message);
		 
					System.out.println("Done");
		 
				} catch (MessagingException e) {
					return false;
					//throw new RuntimeException(e);
				} catch(Exception e){
					return false;
				} 
			return true;
		
	
	}	
/*
 * 	send link 
 * @emailRequest
 */
public boolean sendLinkMessage(String reciever){

		try {
			  // Recipient's email ID needs to be mentioned.
		      String to = reciever;

		      // Sender's email ID needs to be mentioned
		      String from = "postmaster@deltaforceanalytics.com";

		      // Assuming you are sending email from localhost
		      String host = "mail.deltaforceanalytics.com";
		      
		    //  final String username = "sanjayuit@gmail.com";
				//final String password = "g9m8a2i6l4";
		 
		      	final String username = "postmaster@deltaforceanalytics.com";
		      	final String password = "Deltaforce2013";
		      	
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				//props.put("mail.smtp.host", "smtp.gmail.com");
				props.put("mail.smtp.host", host);
				props.put("mail.smtp.port", "587");
		 			//System.out.println("emailRequest.getSendersEmail : " + emailRequest.getSenderEmail());
				Session session = Session.getInstance(props,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				  });
		 
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(from));
					message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(to));
					message.setSubject("DeltaForceAnalytics - Password Recovery Link");
					//message.setText("");
					String textMsg = "<div style=\"background-color:#444;width:626px;height:300px;margin-left:7%;\">"+"<div style=\"margin:0 auto;line-height:18px;padding:0;text-align:center;font-size:12px;font-family:Tahoma,Verdana,Arial,sans-serif;width:600px;\">"+"<div style=\"background-image: linear-gradient(#444, #111);padding:10px;-moz-margin-start:-12px;-webkit-margin-start:-12px;text-align:left;\"><img style=\"border:0px; margin-top: 1%; width=205; height=32;\" src=\"http://dfa-webapp-619760838.us-east-1.elb.amazonaws.com/images/logo.png\" ></div>"+" <div style=\"color:#d2aa56;font-size:16px;padding:8px;text-align:left;\">"
							+"</div>"+"<div style=\"margin-top:20px;text-align:left\">"+" <span style=\"color:#fff;\">Dear Customer,</span>"
							+"<p style=\"color:#fff;\"> You received this email because you filled out a form on DeltaForceAnalytics indicating that you had forgotten your password. <br><br>"
							+"<a style=\"color:#00ca00;\" href='http://localhost:8080/deltaforceanalytics-webapp/pass?"+reciever+"' target='_blank'>click the link to reset your password</a>"
							+"<p style=\"color:#fff;\">Please ignore this email if it wasn't you who requested help with your password - your current password will remain unchanged.</p>"
							+"<p style=\"color:#fff;\">In case you have questions or need further assistance,email us on <a style=\"color:#00ca00;\" >"+"postmaster@deltaforceanalytics.com"+"</a>"+" We would be glad to help you."+"</p>"
	                        + "<p style=\"color:#fff;\">"+" Sincerely,<br>"+" The DeltaForce Team"+"</p>"+"</div>"+"</div>";
					message.setContent(textMsg, "text/html");
			
					Transport.send(message);
		 
					System.out.println("Done");
		 
				} catch (MessagingException e) {
					return false;
					//throw new RuntimeException(e);
				} catch(Exception e){
					return false;
				} 
			return true;
		
	
	}	
	
/*
 * 	sendPasswordConfirm 
 * @emailRequest
 */
public boolean sendPasswordConfirm(String reciever,String Password){

		try {
			  // Recipient's email ID needs to be mentioned.
		      String to = reciever;

		      // Sender's email ID needs to be mentioned
		      String from = "postmaster@deltaforceanalytics.com";

		      // Assuming you are sending email from localhost
		      String host = "mail.deltaforceanalytics.com";
		      
		    //  final String username = "sanjayuit@gmail.com";
				//final String password = "g9m8a2i6l4";
		 
		      	final String username = "postmaster@deltaforceanalytics.com";
		      	final String password = "Deltaforce2013";
		      	
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				//props.put("mail.smtp.host", "smtp.gmail.com");
				props.put("mail.smtp.host", host);
				props.put("mail.smtp.port", "587");
		 			//System.out.println("emailRequest.getSendersEmail : " + emailRequest.getSenderEmail());
				Session session = Session.getInstance(props,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				  });
		 
					Message message = new MimeMessage(session);
					message.setFrom(new InternetAddress(from));
					message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(to));
					message.setSubject("DeltaForceAnalytics - Password Changed");
					//message.setText("");
					String textMsg = "<div style=\"background-color:#444;width:626px;height:250px;margin-left:7%;\">"+"<div style=\"margin:0 auto;line-height:18px;padding:0;text-align:center;font-size:12px;font-family:Tahoma,Verdana,Arial,sans-serif;width:600px;\">"+"<div style=\"background-image: linear-gradient(#444, #111);padding:10px;-moz-margin-start:-12px;-webkit-margin-start:-12px;text-align:left;\"><img style=\"border:0px; margin-top: 1%; width=205; height=32;\" src=\"http://dfa-webapp-619760838.us-east-1.elb.amazonaws.com/images/logo.png\" ></div>"+" <div style=\"color:#d2aa56;font-size:16px;padding:8px;text-align:left;\">"
							+"</div>"+"<div style=\"margin-top:20px;text-align:left\">"+" <span style=\"color:#fff;\">Dear Customer,</span>"
							+"<p style=\"color:#fff;\"> You have Successfully changed your password. <br><br>"
							+" Your login email ID:"+" <span>"+"<a style=\"color:#00ca00;\">"+ reciever+"</a>"+"</span>"+"<br>"
							+" Your login password:"+" <span style=\"color:#00ca00;\">"+ Password+"</span>"+"</p>"
							+"<p style=\"color:#fff;\">In case you have questions or need further assistance,email us on <a style=\"color:#00ca00;\" >"+"postmaster@deltaforceanalytics.com"+"</a>"+" We would be glad to help you."+"</p>"
	                        + "<p style=\"color:#fff;\">"+" Sincerely,<br>"+" The DeltaForce Team"+"</p>"+"</div>"+"</div>";
					message.setContent(textMsg, "text/html");
			
					Transport.send(message);
		 
					System.out.println("Done");
		 
				} catch (MessagingException e) {
					return false;
					//throw new RuntimeException(e);
				} catch(Exception e){
					return false;
				} 
			return true;
		
	
	}	
	
}
