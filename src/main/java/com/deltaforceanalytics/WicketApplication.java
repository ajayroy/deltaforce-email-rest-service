package com.deltaforceanalytics;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.ResourceReference;
import com.deltaforceanalytics.service.rest.DFAMailerRest;


/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 * 
 * @see com.deltaforceanalytics.Start#main(String[])
 */
public class WicketApplication extends WebApplication
{
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();

		// add your configuration here
		
	
				mountResource("/mailrest", new ResourceReference("restJsonReference") {            
		            private static final long serialVersionUID = 1L;
		            DFAMailerRest mailjson = new DFAMailerRest();
		            @Override
		            public IResource getResource() {
		                return mailjson;
		            }

		       });	
	}
}
