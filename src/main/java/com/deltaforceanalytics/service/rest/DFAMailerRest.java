package com.deltaforceanalytics.service.rest;

import org.wicketstuff.rest.annotations.MethodMapping;
import org.wicketstuff.rest.resource.gson.GsonRestResource;

import com.deltaforceanalytics.serviceImpl.DeltaForcemailManagerImpl;


public class DFAMailerRest extends GsonRestResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@MethodMapping(value = "/sendemail/{reciever}/{name}/{Password}")
	public boolean SendEmail(String reciever,String name,String Password) 
	{		
		
		DeltaForcemailManagerImpl dfEmailManager = new DeltaForcemailManagerImpl();
		return dfEmailManager.sendCreateAccountMessage(reciever,name,Password);
	
		
	}
	
	
	@MethodMapping(value = "/sendemaillink/{reciever}")
	public boolean SendEmailLink(String reciever) 
	{		
		
		DeltaForcemailManagerImpl dfEmailManager = new DeltaForcemailManagerImpl();
		return dfEmailManager.sendLinkMessage(reciever);
	
		
	}
	
	@MethodMapping(value = "/sendpassEmail/{reciever}/{Password}")
	public boolean SendPassConfirm(String reciever,String Password) 
	{		
		
		DeltaForcemailManagerImpl dfEmailManager = new DeltaForcemailManagerImpl();
		return dfEmailManager.sendPasswordConfirm(reciever, Password);
	
		
	}
}
